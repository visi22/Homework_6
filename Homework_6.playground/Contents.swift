//: Реализовать структуру IOSCollection и создать в ней copy on write

import Foundation

struct IOSCollection {
    var name = "Io"
}


class Ref <T> {
    var value: T
    init(value: T) {
        self.value = value
    }
}

struct Box<T> {
    var ref: Ref<T>
    init(value: T) {
        self.ref = Ref(value: value)
    }
    
    var value: T {
        get {
            ref.value
        }
        set {
            guard (isKnownUniquelyReferenced(&ref)) else {
                ref = Ref(value: newValue)
                return
            }
            ref.value = newValue
        }
    }
}

var example = IOSCollection()
var first = Box(value: example)
var second = first


print(Unmanaged<AnyObject>.passUnretained(first.ref as AnyObject).toOpaque())

print(Unmanaged<AnyObject>.passUnretained(second.ref as AnyObject).toOpaque())

//: Создать протокол Hotel с инициализатором который принимает roomCount, после создать class HotelAlfa добавить свойство roomCount и подписаться на этот протокол

protocol Hotel {
    init(roomCount: UInt)
}

class HotelAlfa: Hotel {
    var roomCount: UInt
    required init(roomCount: UInt) {
        self.roomCount = roomCount
    }
}

//: Создать protocol GameDice у него {get} свойство numberDice далее нужно расширить Int так что б когда мы напишем такую конструкцию "let diceCoub = 4 \n diceCoub.numberDice" в консоле мы увидели такую строку - "Выпало 4 на кубике"

protocol GameDice {
    var numberDice: String { get }
}

extension Int: GameDice {
    var numberDice: String {
        return "Выпало \(self) на кубике"
    }
}

let diceCoub = 4
diceCoub.numberDice

//: Создать протокол с одним методом и 2 мя свойствами одно из них сделать явно optional, создать класс, подписать на протокол и реализовать только 1 обязательное свойство

@objc protocol Building {
    var floorsCount: Int { get }
    @objc optional var age: Int { get }
    func create()
}


class House: Building {
    var floorsCount: Int
    init(floorsCount: Int) {
        self.floorsCount = floorsCount
    }
    func create() {
        print("I am created")
    }
}

var firstHouse = House(floorsCount: 5)
firstHouse.create()



//: Создать 2 протокола: 3.1 - "Начинай писать код" со свойствами: время, количество кода. И функцией writeCode() 3.2 - "Заканчивай писать код" с функцией: stopCoding() И класс: Разработчик, у которого есть свойства - количество программистов, специализации(ios, android, web). Разработчику подключаем два этих протокола. Задача: вывести в консоль сообщения - "разработка началась. пишем код" и "работа закончена. Сдаю в тестирование".

protocol StartCoding {
    var time: UInt { get set }
    var amountOfCode: UInt { get set }
    func writeCode()
}

protocol EndCoding {
    func stopCoding()
}

enum Specialization {
    case ios
    case android
    case web
}


class Developer: StartCoding, EndCoding {
    let numberOfProgrammers: UInt
    var time: UInt
    var amountOfCode: UInt
    var specialization: Specialization
    
    init(numberOfProgrammers: UInt, time: UInt, amountOfCode: UInt, specialization: Specialization) {
        self.numberOfProgrammers = numberOfProgrammers
        self.time = time
        self.amountOfCode = amountOfCode
        self.specialization = specialization
    }
    
    func writeCode() {
        print("разработка началась. пишем код")
    }
    
    func stopCoding() {
        print("работа закончена. Сдаю в тестирование")
    }
}

var firstTeam = Developer(numberOfProgrammers: 3, time: 100, amountOfCode: 124, specialization: .ios)

firstTeam.writeCode()
firstTeam.stopCoding()
